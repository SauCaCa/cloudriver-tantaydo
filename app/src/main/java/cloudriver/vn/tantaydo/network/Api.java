package cloudriver.vn.tantaydo.network;

import java.util.concurrent.TimeUnit;

import cloudriver.vn.tantaydo.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public class Api {
    private static ServiceInterface serviceInterface;
    private static String HOST = "http://vvcgreen.cloud-river.vn";
    private static int TIME_OUT = 15;

    public static ServiceInterface getService() {
        if (serviceInterface == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(logging);// show log
            }
            builder.readTimeout(TIME_OUT, TimeUnit.SECONDS);
            builder.writeTimeout(TIME_OUT, TimeUnit.SECONDS);
            builder.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
            OkHttpClient client = builder
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(HOST)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            serviceInterface = retrofit.create(ServiceInterface.class);
        }
        return serviceInterface;
    }


    public interface ServiceInterface {
        @FormUrlEncoded
        @POST("/api/login")
        Call<ResponseBody> login(
                @Field("code") String phone_code,
                @Field("password") String password);
    }
}
