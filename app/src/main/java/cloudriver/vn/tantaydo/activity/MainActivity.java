package cloudriver.vn.tantaydo.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import cloudriver.vn.tantaydo.R;
import cloudriver.vn.tantaydo.fragment.HomeFragment;
import cloudriver.vn.tantaydo.fragment.NotificationFragment;
import cloudriver.vn.tantaydo.listener.OnClickNotificationLink;


public class MainActivity extends AppCompatActivity implements OnClickNotificationLink {
    private static final String[] navigationTag = new String[]{"home", "notifications"};


    BottomNavigationView bottomBar;
    BottomNavigationView.OnNavigationItemSelectedListener bottomListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomBar = findViewById(R.id.bottomBar);

        setCurrentTab(0);
        bottomListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                setCurrentTab(item.getItemId() == R.id.bottom_notification ? 1 : 0);
                return false;
            }
        };
        bottomBar.setOnNavigationItemSelectedListener(bottomListener);
    }


    private void setCurrentTab(int position) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //
        Fragment content = fragmentManager.findFragmentByTag(navigationTag[position]);
        if (content != null) {
            if (!content.isVisible())
                transaction.show(content);
        } else
            transaction.add(R.id.container, position == 1
                    ? NotificationFragment.newInstance()
                    : HomeFragment.newInstance(), navigationTag[position]);

        //
        Fragment other = fragmentManager.findFragmentByTag(navigationTag[position == 1 ? 0 : 1]);
        if (other != null)
            transaction.hide(other);

        transaction.commit();
    }

    @Override
    public void onClicked(String url) {
        setCurrentTab(0);
        bottomBar.findViewById(R.id.bottom_home).performClick();
        Fragment fgHome = getSupportFragmentManager().findFragmentByTag(navigationTag[0]);
        if (fgHome != null)
            ((HomeFragment) fgHome).openLink(url);
    }

    @Override
    public void onBackPressed() {

        if (bottomBar.getSelectedItemId() != R.id.bottom_home) {
            bottomBar.findViewById(R.id.bottom_home).performClick();
        } else {
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            Fragment home = fragmentManager.findFragmentByTag(navigationTag[0]);
            if (home != null && home.isVisible()) {
                if (!((HomeFragment) home).onBackPressed())
                    super.onBackPressed();
            } else
                super.onBackPressed();
        }
    }
}
