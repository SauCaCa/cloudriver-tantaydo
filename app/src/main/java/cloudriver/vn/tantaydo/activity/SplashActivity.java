package cloudriver.vn.tantaydo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (TextUtils.isEmpty(getDataPreference().getToken())) {
            startActivity(new Intent(this, LoginActivity.class));
        } else
            startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
