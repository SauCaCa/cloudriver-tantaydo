package cloudriver.vn.tantaydo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cloudriver.vn.tantaydo.R;
import cloudriver.vn.tantaydo.network.Api;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "LoginActivity";
    View group_login, progressBar;
    EditText input_user, input_pass;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        group_login = findViewById(R.id.group_login);
        progressBar = findViewById(R.id.progressBar);

        input_user = findViewById(R.id.input_user);
        input_pass = findViewById(R.id.input_password);

        findViewById(R.id.login_button).setOnClickListener(this);
        input_pass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login();
                    return true;
                }
                return false;
            }
        });
    }

    void showProgress() {
        group_login.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    void hideProgress() {
        group_login.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        login();
    }

    void login() {
        String user = input_user.getText().toString();
        String pass = input_pass.getText().toString();

        boolean isValidate = true;
        if (TextUtils.isEmpty(user)) {
            isValidate = false;
            input_user.setError("Tên đăng nhập không hợp lệ!");
        }
        if (TextUtils.isEmpty(pass)) {
            isValidate = false;
            input_pass.setError("Mật khẩu không hợp lệ!");
        }

        if (isValidate) {
            hideKeyboard();
            showProgress();
            Call<ResponseBody> call = Api.getService().login(user, pass);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.body() != null) {
                            JSONObject jsonObject = new JSONObject(response.body().string());
//                            int result = jsonObject.optInt("result");
                            String token = jsonObject.optString("token");
                            String message = jsonObject.optString("message");
                            if (TextUtils.isEmpty(token)) {
                                showError(message);
                            } else {
                                getDataPreference().setToken(token);
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            }
                        } else showError("Đăng nhập thất bại.\nVui lòng thử lại!");
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        showError(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showError(t.getMessage());
                    t.printStackTrace();
                }
            });

            addCalls(call);
        }
    }

    public void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    void showError(String mess) {
        hideProgress();
        Toast.makeText(LoginActivity.this, mess, Toast.LENGTH_SHORT).show();
    }
}
