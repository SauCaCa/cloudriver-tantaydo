package cloudriver.vn.tantaydo.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import net.orange_box.storebox.StoreBox;

import java.util.ArrayList;
import java.util.List;

import cloudriver.vn.tantaydo.listener.DataPreferences;
import retrofit2.Call;

public class BaseActivity extends AppCompatActivity {
    private List<Call> calls;
    DataPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public DataPreferences getDataPreference() {
        if (preferences == null)
            preferences = StoreBox.create(this, DataPreferences.class);
        return preferences;
    }

    @Override
    protected void onDestroy() {
        callCancel();
        super.onDestroy();
    }

    public void addCalls(Call call) {
        if (calls == null) {
            calls = new ArrayList<>();
        }
        calls.add(call);
    }

    private void callCancel() {
        if (calls != null && calls.size() > 0) {
            for (Call call : calls) {
                if (!call.isCanceled())
                    call.cancel();
            }
            calls.clear();
            calls = null;
        }
    }
}
