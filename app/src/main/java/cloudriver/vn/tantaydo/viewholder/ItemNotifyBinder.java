package cloudriver.vn.tantaydo.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cloudriver.vn.tantaydo.R;
import cloudriver.vn.tantaydo.listener.OnClickNotificationLink;
import me.drakeet.multitype.ItemViewBinder;
import me.saket.bettermovementmethod.BetterLinkMovementMethod;

public class ItemNotifyBinder extends ItemViewBinder<String, ItemNotifyBinder.ViewHolder> {
    OnClickNotificationLink listener;

    public ItemNotifyBinder(OnClickNotificationLink listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_notify, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {
        holder.textView.setText(item);
        BetterLinkMovementMethod.linkify(Linkify.WEB_URLS, holder.textView)
                .setOnLinkClickListener(new BetterLinkMovementMethod.OnLinkClickListener() {
                    @Override
                    public boolean onClick(TextView textView, String url) {
                        if (listener != null)
                            listener.onClicked(url);
                        return true;
                    }
                });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textview);
        }
    }
}
