package cloudriver.vn.tantaydo.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.asksira.webviewsuite.WebViewSuite;

import net.orange_box.storebox.StoreBox;

import cloudriver.vn.tantaydo.R;
import cloudriver.vn.tantaydo.activity.LoginActivity;
import cloudriver.vn.tantaydo.activity.MainActivity;
import cloudriver.vn.tantaydo.listener.DataPreferences;


public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";

    WebViewSuite webViewSuite;
    DataPreferences preferences;
    String token;
    MainActivity activity;
    boolean isLogout = false;


    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_web_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webViewSuite = view.findViewById(R.id.webViewSuite);
        if (activity == null)
            activity = ((MainActivity) getActivity());

        preferences = StoreBox.create(activity, DataPreferences.class);
        token = preferences.getToken();

        webViewSuite.interfereWebViewSetup(new WebViewSuite.WebViewSetupInterference() {
            @Override
            public void interfereWebViewSetup(WebView webView) {
                initWebview(webView);
            }
        });
        webViewSuite.customizeClient(new WebViewSuite.WebViewSuiteCallback() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //Do your own stuffs. These will be executed after default onPageStarted().
                Log.e(TAG, String.format("onPageStarted: %s", url));
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e(TAG, String.format("onPageFinished: %s  - %s", url, isLogout));
                if (TextUtils.equals(url, getString(R.string.home_url)) && isLogout) {
                    preferences.clear();
                    startActivity(new Intent(activity, LoginActivity.class));
                    activity.finish();
                }
                //Do your own stuffs. These will be executed after default onPageFinished().
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //Override those URLs you need and return true.
                //Return false if you don't need to override that URL.
                if (TextUtils.equals(url, "http://vvcgreen.cloud-river.vn/tai-khoan/dang-xuat/")) {
                    isLogout = true;
                }
                Log.e(TAG, String.format("shouldOverrideUrlLoading: %s \n isLogout: %s", url, isLogout));
                return false;
            }
        });
        openLink(getString(R.string.user_url));
    }

    public void openLink(String data) {
        Uri uri = Uri.parse(data)
                .buildUpon()
                .appendQueryParameter("token", token)
                .build();
        webViewSuite.startLoading(uri.toString());
    }

    @SuppressLint("SetJavaScriptEnabled")
    void initWebview(WebView mWebview) {
        mWebview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= 19) {
            mWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            mWebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
        mWebview.getSettings().setLoadsImagesAutomatically(true);
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.getSettings().setJavaScriptEnabled(true);
    }


    public boolean onBackPressed() {
        return webViewSuite.goBackIfPossible();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
