package cloudriver.vn.tantaydo.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cloudriver.vn.tantaydo.R;
import cloudriver.vn.tantaydo.activity.MainActivity;
import cloudriver.vn.tantaydo.viewholder.ItemNotifyBinder;
import me.drakeet.multitype.MultiTypeAdapter;

public class NotificationFragment extends Fragment {
    RecyclerView recyclerView;

    MainActivity mainActivity;
    MultiTypeAdapter adapter;
    List<Object> list;

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recyclerview_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        list = new ArrayList<>();
        adapter = new MultiTypeAdapter();
        adapter.register(String.class, new ItemNotifyBinder(mainActivity));
        recyclerView.setAdapter(adapter);

//        list.add(String.format(getString(R.string.notifications), i));
        list.add(getString(R.string.notification_monthly));
        adapter.setItems(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
