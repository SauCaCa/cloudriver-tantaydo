package cloudriver.vn.tantaydo.listener;

public interface OnClickNotificationLink {
    void onClicked(String url);
}
