package cloudriver.vn.tantaydo.listener;

import net.orange_box.storebox.annotations.method.ClearMethod;
import net.orange_box.storebox.annotations.method.KeyByString;

public interface DataPreferences {
    String KEY_USER = "token";

    @KeyByString(KEY_USER)
    void setToken(String value);

    @KeyByString(KEY_USER)
    String getToken();

    @ClearMethod
    void clear();
}
