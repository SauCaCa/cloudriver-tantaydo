package cloudriver.vn.tantaydo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    protected List<Fragment> mFragmentList;
    protected List<String> mFragmentTitleList;
    FragmentManager manager;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
        mFragmentList = new ArrayList<>();
        mFragmentTitleList = new ArrayList<>();
        this.manager = manager;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        if (mFragmentList == null) mFragmentList = new ArrayList<>();
        if (mFragmentTitleList == null) mFragmentTitleList = new ArrayList<>();
        if (!mFragmentTitleList.contains(title) || TextUtils.isEmpty(title)) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public void clear() {
        if (mFragmentList != null) {
            mFragmentList.clear();
        }
        if (mFragmentTitleList != null)
            mFragmentTitleList.clear();
    }
}